from .diving3d import *
from .califa import *
from .manga import *
from .muse import *
from .gmos import *

read_type = {'diving3d': read_diving3d,
             'califa': read_califa,
             'manga': read_manga,
             'muse': read_muse,
             'gmos': read_gmos,
             }